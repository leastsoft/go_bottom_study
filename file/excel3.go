package main

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
)

func main() {
	f, err := excelize.OpenFile( "/Users/mac/Desktop/golang/go_bottom_study/file/example_iteration.xlsx")


	f.MergeCell("First Sheet", "A1", "B2")
	//f.MergeCell("Sheet1", "D9", "E9")
	//f.MergeCell("Sheet1", "H14", "G13")
	//f.MergeCell("Sheet1", "C9", "D8")
	//f.MergeCell("Sheet1", "F11", "G13")
	//f.MergeCell("Sheet1", "H7", "B15")
	//f.MergeCell("Sheet1", "D11", "F13")
	//f.MergeCell("Sheet1", "G10", "K12")
	f.SetCellValue("First Sheet", "G11", "set value in merged cell")
	//f.SetCellInt("Sheet1", "H11", 100)
	//f.SetCellValue("Sheet1", "I11", float64(0.5))
	//f.SetCellHyperLink("Sheet1", "J11", "https://github.com/360EntSecGroup-Skylar/excelize", "External")
	//f.SetCellFormula("Sheet1", "G12", "SUM(Sheet1!B19,Sheet1!C19)")
	//f.GetCellValue("Sheet1", "H11")
	//f.GetCellValue("Sheet2", "A6") // Merged cell ref is single coordinate.
	//f.GetCellFormula("Sheet1", "G12")
	f.Save()
	fmt.Println(err)
}
